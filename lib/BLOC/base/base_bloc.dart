import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_file_manager/Utils/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'base_event.dart';
part 'base_state.dart';

class BaseBloc<T extends BaseEvent, Y extends BaseInitial> extends Bloc<T, Y>  with WidgetsBindingObserver{
  final BuildContext? context;
  final bool? isAutoDelToken;
  BaseBloc(Y baseInitial, {this.context, this.isAutoDelToken}) : super(baseInitial) {
    SharedPreference.onCheckTokenLocal(context!);
    WidgetsBinding.instance?.addObserver(this);
    on<T>((event, emit) {

    });
  }

  @override
  Future<void> close() {
    WidgetsBinding.instance?.removeObserver(this);
    return super.close();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    _initLifecycle(state);
    super.didChangeAppLifecycleState(state);
  }

  void _initLifecycle(AppLifecycleState state){
    switch(state) {
      case AppLifecycleState.resumed:
        SharedPreference.onCheckTokenLocal(context!);
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }
}

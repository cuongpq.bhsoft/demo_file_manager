import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:demo_file_manager/BLOC/filedetail/file_detail_screen.dart';
import 'package:demo_file_manager/BLOC/open_detail/file_open_detail.dart';
import 'package:demo_file_manager/BLOC/player_audio/play_audio_screen.dart';
import 'package:demo_file_manager/Utils/util.dart';
import 'package:demo_file_manager/model/item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
part 'file_detail_event.dart';
part 'file_detail_state.dart';

class FileDetailBloc extends Bloc<FileDetailEvent, FileDetailInitial> {
  final BuildContext? context;
  List<Item> listChil = [];

  FileDetailBloc({this.context}) : super(FileDetailInitial()) {
    on<FileDetailEvent>((event, emit) async {
      if (event is ListDetailFileEvent) {
        Directory dir = Directory(event.parent!.path!);
        List<FileSystemEntity> childFiles = dir.listSync(recursive: false);
        listChil = GlobalUtil.mapListFileToListItem(childFiles);
        emit.call(state.updateState());
      }
      if (event is OpenFileEvent) {
        _onOpenFile(event.item!);
      }
    });
  }

  Future _onOpenFile(Item item) async {
    if (GlobalUtil.audioType.contains(item.type) || GlobalUtil.videoType.contains(item.type)) {
      Navigator.of(context!).push(MaterialPageRoute(builder: (context) => PlayAudio(item: item)));
    }else if (GlobalUtil.folder.contains(item.type)) {
      Navigator.of(context!).push(MaterialPageRoute(builder: (context)=>FileDetailScreen(parent: item)));
    }else{
      Navigator.of(context!).push(MaterialPageRoute(builder: (context) => FileOpenDetail(item: item)));
    }
  }
}

import 'package:demo_file_manager/BLOC/filedetail/file_detail_bloc.dart';
import 'package:demo_file_manager/model/item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FileDetailScreen extends StatelessWidget {
  final Item? parent;

  const FileDetailScreen({Key? key, this.parent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(parent!.title!),
      ),
      body: BlocProvider(
        create: (context) => FileDetailBloc(context: context)
          ..add(ListDetailFileEvent(parent: parent)),
        child: BlocBuilder<FileDetailBloc, FileDetailInitial>(
          builder: (context, fileState) {
            FileDetailBloc bloc = BlocProvider.of<FileDetailBloc>(context);
            return _buildListView(context: context, bloc: bloc);
          },
        ),
      ),
    );
  }

  Widget _buildListView(
      {required BuildContext context, required FileDetailBloc bloc}) {
    return ListView.builder(
        itemCount: bloc.listChil.length,
        itemBuilder: (context, index) {
          return _itemListView(
              context: context, item: bloc.listChil[index], bloc: bloc);
        });
  }

  Widget _itemListView(
      {required BuildContext context,
      required Item? item,
      required FileDetailBloc bloc}) {
    return InkWell(
      onTap: () {
        bloc.add(OpenFileEvent(item: item));
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(5.0),
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  item!.icon!,
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 0, horizontal: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(5.0),
                          width: MediaQuery.of(context).size.width * 0.7,
                          child: Text(
                            item.title!,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            style: const TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Row(
                          children: [
                            Text(
                              item.type!,
                              style: const TextStyle(
                                  fontSize: 12, fontStyle: FontStyle.italic),
                            ),
                          ],
                        ),
                        Container(
                          padding: const EdgeInsets.all(5.0),
                          width: MediaQuery.of(context).size.width * 0.7,
                          child: Text(
                            item.path!,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            style: const TextStyle(fontSize: 13),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const Divider(
              height: 10,
              color: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }
}

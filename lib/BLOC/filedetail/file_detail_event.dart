part of 'file_detail_bloc.dart';

@immutable
abstract class FileDetailEvent {
  const FileDetailEvent();
  List<Object?> get props =>[];
}

class ListDetailFileEvent extends FileDetailEvent{
  final Item? parent;
  const ListDetailFileEvent({this.parent});
  @override
  List<Object?> get props => [parent];
}

class OpenFileEvent extends FileDetailEvent{
  final  Item? item;
  const OpenFileEvent({this.item});
  @override
  List<Object?> get props => [item];
}

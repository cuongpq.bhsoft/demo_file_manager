part of 'file_detail_bloc.dart';

@immutable
abstract class FileDetailState {}

class FileDetailInitial extends FileDetailState {

  FileDetailInitial();

  FileDetailInitial updateState(){
    return FileDetailInitial();
  }

}

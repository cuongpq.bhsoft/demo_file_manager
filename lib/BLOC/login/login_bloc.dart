
import 'package:bloc/bloc.dart';
import 'package:demo_file_manager/BLOC/welcome/welcome_screen.dart';
import 'package:demo_file_manager/Utils/common.dart';
import 'package:demo_file_manager/Utils/shared_preferences.dart';
import 'package:demo_file_manager/model/user.dart';
import 'package:demo_file_manager/repository/my_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginInitial> {
  final MyRepository _repository = MyRepositoryImpl();
  final BuildContext? context;
  String? password = "";
  String? confirmPassword = "";
  bool isRegistered = false;

  LoginBloc({this.context}) : super(LoginInitial()) {
    on<LoginEvent>((event, emit) async {
      if (event is OnCheckLogin) {
        List<User>? users = await _repository.getAllUser();
        var len = users?.length ?? 0;
        if (len != 0) {
          isRegistered = true;
          emit.call(state.updateState());
        }
      }

      if (event is OnLoginEvent) {
        List<User>? users = await _repository.getAllUser();
        if (users!.isNotEmpty) {
          if (password == null) {
            return;
          }
          bool isSuccess =
              await Common.verifyPassword(users[0].password!, password!);
          if (isSuccess) {
            SharedPreference.setTokenShared(
                await Common.bcryptEncodePassword(password!));
            Navigator.of(context!).push(MaterialPageRoute(
              builder: (context) => const WelcomeScreen(),
            ));
          }
        }
      }

      if (event is OnRegisterEvent) {
        if (password == "" ||
            confirmPassword == "" ||
            confirmPassword != password) {
          return;
        }
        String passEncode = await Common.bcryptEncodePassword(password!);
        User user = User(password: passEncode);
        _repository.savePassword(user);
        SharedPreference.setTokenShared(passEncode);
        Navigator.of(context!).push(MaterialPageRoute(
          builder: (context) => const WelcomeScreen(),
        ));
      }

      if (event is OnChangePassword) {
        if (event.password != null) {
          password = event.password;
        }
        if (event.confirmPassword != null) {
          confirmPassword = event.confirmPassword;
        }
      }
    });
  }
  @override
  Future<void> close() {
    _repository.db.close();
    return super.close();
  }
}

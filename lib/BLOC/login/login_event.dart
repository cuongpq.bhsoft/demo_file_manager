part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class OnCheckLogin extends LoginEvent{
  OnCheckLogin();
}

class OnLoginEvent extends LoginEvent{
  final String? password;
  OnLoginEvent({this.password});

}

class OnRegisterEvent extends LoginEvent{
  OnRegisterEvent();

}

class OnChangePassword extends LoginEvent{
  final String? password;
  final String? confirmPassword;
  OnChangePassword({this.password, this.confirmPassword});

}
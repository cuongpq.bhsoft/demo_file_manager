

import 'package:demo_file_manager/BLOC/login/login_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatelessWidget{
  const LoginScreen({Key? key}): super(key: key);
  @override
  Widget build(BuildContext context) {

    return BlocProvider(
      create: (context) => LoginBloc(context: context)..add(OnCheckLogin()),
      child: BlocBuilder<LoginBloc, LoginInitial>(builder: (context, state) {
        LoginBloc bloc = BlocProvider.of<LoginBloc>(context);
        return Scaffold(
          appBar: null,
          body: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16.0),
                color: Colors.blue
            ),
            child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 16.0),
                child: bloc.isRegistered ? _buildWidgetLogin(bloc: bloc) : _buildWidgetRegister(bloc: bloc)
            ),
          ),
        );
      },),
    );
  }
  Widget _buildWidgetLogin({required LoginBloc bloc}){
    return  ListView(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 200, bottom: 50.0),
          child: const Center(
            child: Text('File security', style: TextStyle(color: Colors.white, fontSize: 26.0, fontWeight: FontWeight.bold),),
          ),
        ),
        TextFormField(
          obscureText: true,
          enableSuggestions: false,
          autocorrect: false,
          style: const TextStyle(fontSize: 14.0, color: Color(0xFFbdc6cf)),
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            hintText: 'Input your password',
            border: const OutlineInputBorder(),
            contentPadding:
            const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(25.7),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: const BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(25.7),
            ),
            suffixIcon: IconButton(
              onPressed: () {
                bloc.add(OnLoginEvent());
              },
              icon: const Icon(Icons.login),
            ),
          ),
          onChanged: (value) {
            bloc.add(OnChangePassword(password: value));
          },
        )
      ],
    );
  }

  Widget _buildWidgetRegister({required LoginBloc bloc}){
    return  ListView(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 200, bottom: 50.0),
          child: const Center(
            child: Text('Set your password', style: TextStyle(color: Colors.white, fontSize: 26.0, fontWeight: FontWeight.bold),),
          ),
        ),
        TextFormField(
            obscureText: true,
            enableSuggestions: false,
            autocorrect: false,
            style: const TextStyle(fontSize: 14.0, color: Color(0xFFbdc6cf)),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintText: 'Input your password',
              border: const OutlineInputBorder(),
              contentPadding:
              const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(25.7),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: const BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(25.7),
              ),
              suffixIcon: IconButton(
                onPressed: () {
                  bloc.add(OnLoginEvent());
                },
                icon: const Icon(Icons.clear),
              ),
            ),
            onChanged: (value) {
              bloc.add(OnChangePassword(password: value ));
            },
          ),
        Container(
          margin: const EdgeInsets.only(top: 20),
          child: TextFormField(
            obscureText: true,
            enableSuggestions: false,
            autocorrect: false,
            style: const TextStyle(fontSize: 14.0, color: Color(0xFFbdc6cf)),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintText: 'Confirm your password',
              border: const OutlineInputBorder(),
              contentPadding:
              const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(25.7),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: const BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(25.7),
              ),
              suffixIcon: IconButton(
                onPressed: () {
                  bloc.add(OnLoginEvent());
                },
                icon: const Icon(Icons.clear),
              ),
            ),
            onChanged: (value) {
              bloc.add(OnChangePassword(confirmPassword: value ));
            },
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20),
          child: IconButton(
            icon: const Icon(Icons.login_sharp),
            color: Colors.white,
            onPressed: () {
              bloc.add(OnRegisterEvent());
            },
          ),
        )
      ],
    );
  }
}


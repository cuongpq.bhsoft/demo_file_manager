import 'package:demo_file_manager/BLOC/base/base_bloc.dart';
import 'package:demo_file_manager/BLOC/welcome/welcome_bloc.dart';
import 'package:demo_file_manager/Utils/nav_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => WelcomeBloc(context),
      child: BlocBuilder<WelcomeBloc, BaseInitial>(builder: (context, state) {
        return Scaffold(
          drawer: const NavDrawer(),
          appBar: AppBar(
            title: const Text('Welcome'),
          ),
          body: ListView(
              children: [
                Image.asset('images/fsecure.jpg'),
                Container(
                  margin: const EdgeInsets.only(top: 50.0),
                  child: const Align(alignment: Alignment.center,
                    child: Text(
                      'Welcome to File security', style: TextStyle(color: Colors
                        .blue, fontWeight: FontWeight.bold, fontSize: 26.0),),),
                )
              ],
            ),
        );
      },
      ),
    );
  }

}
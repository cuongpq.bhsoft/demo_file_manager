
import 'package:demo_file_manager/BLOC/base/base_bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'welcome_event.dart';
part 'welcome_state.dart';

class WelcomeBloc extends BaseBloc<BaseEvent, WelcomeInitial> {
  WelcomeBloc(BuildContext context) : super(WelcomeInitial(), context: context, isAutoDelToken: true) {
    on<WelcomeEvent>((event, emit) {
      if(event is TestEvent){
      }
    });
  }
}

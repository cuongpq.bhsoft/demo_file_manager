import 'package:bloc/bloc.dart';
import 'package:demo_file_manager/Utils/util.dart';
import 'package:demo_file_manager/model/item.dart';
import 'package:meta/meta.dart';
import 'package:open_file/open_file.dart';

part 'file_open_detail_event.dart';
part 'file_open_detail_state.dart';

class FileOpenDetailBloc extends Bloc<FileOpenDetailEvent, FileOpenDetailInitial> {
  final Item? item;
  FileOpenDetailBloc({this.item}) : super(FileOpenDetailInitial()) {
    on<FileOpenDetailEvent>((event, emit) {
    });
  }


  int checkIsTypeFile() {
    String type = item!.type!.toLowerCase();
    if (GlobalUtil.audioType.contains(type)) {
      return GlobalUtil.isAUDIOType;
    }
    if (GlobalUtil.wordType.contains(type)) {
      return GlobalUtil.isWORDType;
    }
    if (GlobalUtil.exelType.contains(type)) {
      return GlobalUtil.isEXELType;
    }
    if (GlobalUtil.pdfType.contains(type)) {
      return GlobalUtil.isPDFType;
    }
    if (GlobalUtil.videoType.contains(type)) {
      return GlobalUtil.isVIDEOType;
    }
    if (GlobalUtil.imageType.contains(type)) {
      return GlobalUtil.isIMAGEType;
    }
    if (GlobalUtil.folder.contains(type)) {
      return GlobalUtil.isFOLDER;
    }
    return GlobalUtil.isOther;
  }

  void onOpenOut(){
    OpenFile.open(item?.path);
  }
}

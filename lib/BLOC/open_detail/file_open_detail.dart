import 'dart:io';
import 'package:demo_file_manager/BLOC/open_detail/file_open_detail_bloc.dart';
import 'package:demo_file_manager/Utils/util.dart';
import 'package:demo_file_manager/model/item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';

class FileOpenDetail extends StatelessWidget {
  final Item? item;
  const FileOpenDetail({Key? key, this.item}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FileOpenDetailBloc(item: item),
      child: BlocBuilder<FileOpenDetailBloc, FileOpenDetailInitial>(
        builder: (context, state) {
          FileOpenDetailBloc bloc = BlocProvider.of<FileOpenDetailBloc>(context);
          return Scaffold(
            appBar: AppBar(
              title: Text(bloc.item!.title!),
            ),
            body: Container(
                padding: const EdgeInsets.all(25.0),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: (() {
                    if (bloc.checkIsTypeFile() == GlobalUtil.isIMAGEType) {
                      return _buildImageView(context: context,bloc: bloc);
                    } else if (bloc.checkIsTypeFile() ==
                        GlobalUtil.isPDFType) {
                      return _buildPdfView(bloc: bloc);
                    } else {
                      return _buildOpenOutFile(bloc: bloc);
                    }
                  }()),
                )),
          );
        },
      ),
    );
  }

  Widget _buildImageView({required BuildContext context, required FileOpenDetailBloc bloc}) {
    return Image.file(
      File(bloc.item!.path!),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
    );
  }

  Widget _buildPdfView({required FileOpenDetailBloc bloc}) {
    return PDFView(
      filePath: bloc.item!.path!,
      enableSwipe: true,
      swipeHorizontal: true,
      autoSpacing: false,
      pageFling: false,
      onRender: (_pages) {},
      onError: (error) {
      },
      onPageError: (page, error) {
      },
      onViewCreated: (PDFViewController pdfViewController) {},
      onPageChanged: (page, total) {
      },
    );
  }
  Widget _buildOpenOutFile({required FileOpenDetailBloc bloc}){
    bloc.onOpenOut();
    return Center(
        child: Text(bloc.item!.title!, style: const TextStyle(color: Colors.blue, fontSize: 18.0,fontWeight: FontWeight.bold),),
    );
  }
}

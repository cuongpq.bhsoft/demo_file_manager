import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:demo_file_manager/model/item.dart';
import 'package:flutter/cupertino.dart';
import 'package:just_audio/just_audio.dart';
import 'package:meta/meta.dart';
import 'package:video_player/video_player.dart';

part 'play_audio_event.dart';
part 'play_audio_state.dart';

class PlayAudioBloc extends Bloc<PlayAudioEvent, PlayAudioInitial> {
  late final AudioPlayer audioPlayer;
  final BuildContext? context;
  final Item? item;
   bool isPlaying = false;

  late final VideoPlayerController videoPlayerController;
  PlayAudioBloc({this.context, this.item})
      : super(PlayAudioInitial()) {
    audioPlayer = AudioPlayer();
    videoPlayerController  = VideoPlayerController.file(File(item!.path!))..initialize().then((_) {});
    on<PlayAudioEvent>((event, emit) async {
      if (event is PlayEvent) {
        isPlaying = !isPlaying;
        if (isPlaying) {
          _playMusic(event.item!.path!);
        }else{
          _pauseMusic();
        }
        emit.call(state.updateState());
      }
      if (event is DisposeEvent) {
        _disposeMusic();
      }
      if(event is StopEvent){
        isPlaying = !isPlaying;
        _stopMusic();
        emit.call(state.updateState());
      }
      if(event is PlayVideoEvent){
        isPlaying = !isPlaying;
        videoPlayerController.value.isPlaying
            ? videoPlayerController.pause()
            : videoPlayerController.play();
        emit.call(state.updateState());
      }
    });
  }

  @override
  Future<void> close() async {
    audioPlayer.dispose();
    videoPlayerController.dispose();
    super.close();
  }

  Future _playMusic(String path) async {
    await audioPlayer.stop();
    await audioPlayer.setFilePath(path);
    await audioPlayer.play();
  }

  Future _pauseMusic() async {
    await audioPlayer.pause();
  }

  Future _stopMusic() async {
    await audioPlayer.stop();
  }

  Future _disposeMusic() async {
    await audioPlayer.dispose();
  }
}

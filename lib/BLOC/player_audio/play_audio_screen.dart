import 'package:demo_file_manager/BLOC/player_audio/play_audio_bloc.dart';
import 'package:demo_file_manager/Utils/util.dart';
import 'package:demo_file_manager/model/item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';

class PlayAudio extends StatelessWidget{
  final Item? item;
  const PlayAudio({Key? key,this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PlayAudioBloc(context: context,item: item),
      child: BlocBuilder<PlayAudioBloc, PlayAudioInitial>(
        builder: (context, state) {
          PlayAudioBloc bloc = BlocProvider.of<PlayAudioBloc>(context);
          return Scaffold(
              appBar: AppBar(
                title: const Text('Play'),
              ),
              body: (() {
                if (GlobalUtil.videoType.contains(bloc.item!.type)) {
                  return _buildVideoWidget(bloc: bloc);
                } else {
                  return Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const IconButton(
                            onPressed: null,
                            icon: Icon(Icons.skip_previous),
                            iconSize: 30,
                          ),
                          IconButton(
                            onPressed: () {
                              BlocProvider.of<PlayAudioBloc>(context).add(
                                  PlayEvent(
                                      item: item,
                                      isPlay: bloc.isPlaying));
                            },
                            icon: bloc.isPlaying
                                ? const Icon(Icons.pause)
                                : const Icon(Icons.play_arrow),
                            iconSize: 30,
                          ),
                          const IconButton(
                            onPressed: null,
                            icon: Icon(Icons.skip_next),
                            iconSize: 30,
                          ),
                          IconButton(
                            onPressed: () {
                              BlocProvider.of<PlayAudioBloc>(context)
                                  .add(StopEvent());
                            },
                            icon: const Icon(Icons.stop),
                            iconSize: 30,
                          ),
                        ],
                      ),
                    );
                }
              }()));
        },
      ),
    );
  }
  Widget _buildVideoWidget({required PlayAudioBloc bloc}) {
    return Stack(
      children: <Widget>[
        Positioned(
            child: Align(
              alignment: Alignment.center,
              child: Center(
                child: bloc.videoPlayerController.value.isInitialized
                    ? AspectRatio(
                  aspectRatio: bloc.videoPlayerController.value.aspectRatio,
                  child: VideoPlayer(bloc.videoPlayerController),
                )
                    : Container(),
              ),
            )),
        Positioned(
          child: Align(
            alignment: Alignment.center,
            child: Container(
              color: Colors.white,
              child: IconButton(
                onPressed: () {
                  bloc.add(const PlayVideoEvent());
                },
                icon: bloc.videoPlayerController.value.isPlaying
                    ? const Icon(Icons.pause)
                    : const Icon(Icons.play_arrow),
                iconSize: 30,
              ),
            ),
          ),
        )
      ],
    );
  }
}

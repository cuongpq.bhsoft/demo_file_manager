part of 'play_audio_bloc.dart';

@immutable
abstract class PlayAudioEvent {
  const PlayAudioEvent();
  List<Object?> get props => [];
}

class PlayEvent extends PlayAudioEvent {
  final Item? item;
  final bool? isPlay;

  const PlayEvent({this.item, this.isPlay});
  @override
  List<Object?> get props => [item, isPlay];
}
class DisposeEvent extends PlayAudioEvent {
  @override
  List<Object?> get props => [];
}

class StopEvent extends PlayAudioEvent{
  @override
  List<Object?> get props => [];
}

class PlayVideoEvent extends PlayAudioEvent {
  final Item? item;
  final bool? isPlay;
  const PlayVideoEvent({this.item, this.isPlay});
  @override
  List<Object?> get props => [item, isPlay];
}
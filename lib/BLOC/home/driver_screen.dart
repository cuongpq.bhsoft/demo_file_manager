import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:confirm_dialog/confirm_dialog.dart';
import 'package:demo_file_manager/BLOC/home/file_bloc.dart';
import 'package:demo_file_manager/Utils/constants.dart';
import 'package:demo_file_manager/Utils/nav_drawer.dart';
import 'package:demo_file_manager/Utils/util.dart';
import 'package:demo_file_manager/model/item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FileBloc(context: context)..add(GetListFileEvent()),
      child: BlocBuilder<FileBloc, FileStateInitial>(
        builder: (context, fileState) {
          FileBloc bloc = BlocProvider.of<FileBloc>(context);
          return _buildListView(context, bloc);
        },
      ),
    );
  }

  Widget _buildListView(BuildContext context, FileBloc bloc) {
    return Scaffold(
        drawer: const NavDrawer(),
        appBar: AppBar(
          title: const Text("Driver"),
          actions: <Widget>[
            (() {
              if (bloc.isStatus == GlobalUtil.selectedStatus) {
                return TextButton(
                    onPressed: () async {
                      if (await confirm(context)) {
                        bloc.add(
                            DeleteMutilFile(deleteItems: bloc.listSelected));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Cancel')));
                      }
                    },
                    child: const Text(
                      'Delete',
                      style: TextStyle(color: Colors.white),
                    ));
              } else {
                return Container();
              }
            }()),
            IconButton(
                onPressed: () {
                  bloc.add(const OnToggleSelected());
                },
                icon: bloc.isStatus == GlobalUtil.normalStatus
                    ? const Icon(Icons.radio_button_checked)
                    : const Icon(Icons.done)),
            PopupMenuButton<String>(onSelected: (value) {
              menuAction(item: value, bloc: bloc);
            }, itemBuilder: (context) {
              return Constants.menus.map((String menu) {
                return PopupMenuItem<String>(value: menu, child: Text(menu));
              }).toList();
            })
          ],
        ),
        body: (() {
          if (bloc.listFiles.isEmpty) {
            return Center(
                child: ListView(
              children: [
                Container(
                  child: const Align(
                    alignment: Alignment.center,
                    child: Text('Your driver dont have item!',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.grey)),
                  ),
                  margin: const EdgeInsets.only(top: 100.0, bottom: 50.0),
                ),
                IconButton(
                  icon: const Icon(
                    Icons.download_outlined,
                    color: Colors.blue,
                    size: 50.0,
                  ),
                  onPressed: () {
                    bloc.add(AddNewFile());
                  },
                )
              ],
            ));
          } else {
            return ListView.builder(
                itemCount: bloc.listFiles.length,
                itemBuilder: (context, index) {
                  return _itemListView(
                      context: context,
                      item: bloc.listFiles[index],
                      bloc: bloc);
                });
          }
        }()));
  }

  Widget _itemListView(
      {required BuildContext context,
      required Item item,
      required FileBloc bloc}) {
    return InkWell(
      onTap: () {
        if (bloc.isStatus == GlobalUtil.selectedStatus) {
          bloc.add(OnChangeItemSelected(item: item));
        } else {
          bloc.add(OpenFolder(item: item));
        }
      },
      onLongPress: () {
        bloc.add(OnLongPressItem(item: item));
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        child: Column(
          children: [
            Row(
              children: [
                Column(
                  children: (() {
                    return [
                      bloc.isStatus == GlobalUtil.normalStatus
                          ? item.icon!
                          : bloc.listSelected.contains(item)
                              ? const Icon(Icons.radio_button_checked,
                                  color: Colors.blue)
                              : const Icon(Icons.radio_button_off)
                    ];
                  }()),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.8,
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              item.title!,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              textAlign: TextAlign.left,
                              style: const TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            item.type!,
                            style: const TextStyle(
                                fontSize: 12, fontStyle: FontStyle.italic),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.all(5.0),
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: Text(
                              item.path!,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              textAlign: TextAlign.left,
                              style: const TextStyle(fontSize: 13),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const Divider(
              height: 10,
              color: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }

  void menuAction({required String item, required FileBloc bloc}) {
    if (item == Constants.add) {
      bloc.add(AddNewFile());
    } else if (item == Constants.refresh) {
      bloc.add(GetListFileEvent());
    }
  }

  void menuSelected({required String item, required FileBloc bloc}) {
    if (item == Constants.delete) {}
  }
}

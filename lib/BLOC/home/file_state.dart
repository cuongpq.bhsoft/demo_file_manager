part of 'file_bloc.dart';

@immutable
abstract class FileState {}

class FileStateInitial extends BaseInitial {
  FileStateInitial();
  @override
  FileStateInitial updateState() {
    return FileStateInitial();
  }

}

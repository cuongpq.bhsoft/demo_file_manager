import 'dart:async';
import 'dart:io';
import 'package:demo_file_manager/BLOC/base/base_bloc.dart';
import 'package:demo_file_manager/Utils/shared_preferences.dart';
import 'package:demo_file_manager/Utils/util.dart';
import 'package:demo_file_manager/model/item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import '../filedetail/file_detail_screen.dart';
part 'file_event.dart';
part 'file_state.dart';

class FileBloc extends BaseBloc<BaseEvent, FileStateInitial> {
  int isStatus = GlobalUtil.normalStatus;
  final List<Item> listSelected = List.empty(growable: true);
  final List<Item> listFiles = List<Item>.empty(growable: true);

  FileBloc({context}) : super(FileStateInitial(), context: context, isAutoDelToken: false) {
    on<FileEvent>((event, emit) async {
      if (event is GetListFileEvent) {
        listFiles.addAll((await getListFile())!);
        emit.call(state.updateState());
      }

      if (event is OpenFolder) {
        Navigator.of(context!).push(MaterialPageRoute(
            builder: (context) => FileDetailScreen(
              parent: event.item!,
                )));
      }

      if (event is AddNewFile) {
        await GlobalUtil.onSelectFile(context: context).then((value) async {
          listFiles.addAll((await getListFile())!);
          emit.call(state.updateState());
        });
      }

      if (event is DeleteMutilFile) {
        List<Item>? deleteLists = event.deleteItems;
        if (deleteLists!.isNotEmpty) {
          for (var item in deleteLists) {
            if (GlobalUtil.folder.contains(item.type)) {
              Directory(item.path!).deleteSync(recursive: true);
            } else {
              File(item.path!).deleteSync(recursive: true);
            }
          }
          isStatus = GlobalUtil.normalStatus;
          listSelected.clear();
          listFiles.clear();
          listFiles.addAll((await getListFile())!);
          emit.call(state.updateState());
        }
      }
      if (event is OnToggleSelected) {
        if (isStatus == GlobalUtil.selectedStatus) {
          isStatus = GlobalUtil.normalStatus;
          listSelected.clear();
        } else {
          isStatus = GlobalUtil.selectedStatus;
        }
        emit.call(state.updateState());
      }
      if (event is OnChangeItemSelected) {
        if (listSelected.contains(event.item)) {
          listSelected.remove(event.item);
        } else {
          listSelected.add(event.item!);
        }
        emit.call(state.updateState());
      }
      if (event is OnLongPressItem) {
        isStatus = GlobalUtil.selectedStatus;
        listSelected.add(event.item!);
        emit.call(state.updateState());
      }
    });
  }

  Future<List<Item>?> getListFile() async {
    var status = await Permission.storage.status;
    if (status.isDenied) {
      await Permission.storage.request();
    }
    Directory directory = await getExternalSdCardPath();
     List<FileSystemEntity> files = [];
    if (await Permission.storage.request().isGranted) {
      files = directory.listSync();
    }
    return GlobalUtil.mapListFileToListItem(files);
  }

  Future<Directory> getExternalSdCardPath() async {
    final Directory appDocDir = await getApplicationDocumentsDirectory();
    String appPath = appDocDir.path;
    appPath = appPath + "/" + GlobalUtil.folderDocument;
    bool isDirExits = await Directory(appPath).exists();
    if (!isDirExits) {
      Future<Directory> newDr = Directory(appPath).create(recursive: true);
      return newDr;
    }
    return Directory(appPath);
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    _initLifecycle(state);
    super.didChangeAppLifecycleState(state);
  }

  void _initLifecycle(AppLifecycleState state){
    switch(state) {
      case AppLifecycleState.resumed:
        SharedPreference.onCheckTokenLocal(context!);
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }
}

part of 'file_bloc.dart';

@immutable
abstract class FileEvent extends BaseEvent{
  const FileEvent();
}


class GetListFileEvent extends FileEvent{

  @override
  List<Object?> get props => [];
}
class OpenFolder extends FileEvent{
  final Item? item;
  const OpenFolder({this.item});

  @override
  List<Object?> get props => [item];
}

class AddNewFile extends FileEvent{
  @override
  List<Object?> get props => [];
}

class DeleteMutilFile extends FileEvent{
  final List<Item>? deleteItems;
  const  DeleteMutilFile({this.deleteItems});
  @override
  List<Object?> get props => [deleteItems];
}

class OnToggleSelected extends FileEvent{
  final int? isStatus;
  const OnToggleSelected({this.isStatus});
  @override
  List<Object?> get props => [isStatus];
}

class OnChangeItemSelected extends FileEvent{
  final Item? item;
  const OnChangeItemSelected({this.item});
  @override
  List<Object?> get props => [item];
}
class OnLongPressItem extends FileEvent{
  final Item? item;
  const OnLongPressItem({this.item});
  @override
  List<Object?> get props => [item];
}
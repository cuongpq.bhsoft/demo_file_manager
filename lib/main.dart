
import 'package:demo_file_manager/BLOC/welcome/welcome_screen.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(const DriverApp());
}

class DriverApp extends StatelessWidget{
  const  DriverApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const WelcomeScreen()
    );
  }
}


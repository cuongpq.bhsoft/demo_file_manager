import 'dart:io';

import 'package:demo_file_manager/model/item.dart';
import 'package:demo_file_manager/repository/my_repository.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class GlobalUtil {
  static const int normalStatus = 2101;
  static const int selectedStatus = 2102;

  static List<Item> mapListFileToListItem(List<FileSystemEntity>? childFiles) {
    final listItems = <Item>[];
    for (var e in childFiles!) {
      String title =
          e.path.substring(e.path.lastIndexOf('/') + 1, e.path.length);

      String type = title
          .substring(title.lastIndexOf('.') + 1, title.length)
          .toLowerCase();
      if (!title.contains(".") ) {
        type = "folder";
      }
      listItems.add(Item(
          title: title,
          type: type,
          icon: getIconItem(title, type, path: e.path),
          path: e.path));
    }
    return listItems;
  }

  static Widget? getIconItem(String title, String type,{String? path}) {
    if (GlobalUtil.audioType.contains(type)) {
      return const Icon(Icons.audiotrack_outlined);
    }
    if (GlobalUtil.videoType.contains(type)) {
      return const Icon(Icons.video_call_sharp);
    }
    if (GlobalUtil.pdfType.contains(type)) {
      return const Icon(Icons.picture_as_pdf_sharp);
    }
    if (GlobalUtil.wordType.contains(type)) {
      return const Icon(Icons.text_snippet_outlined);
    }
    if (GlobalUtil.exelType.contains(type)) {
      return const Icon(Icons.text_snippet_outlined);
    }
    if (GlobalUtil.imageType.contains(type)) {
      if(path != null){
        return Image.file(File(path), width: 30, height: 30);
      }
      return const Icon(Icons.image_outlined);
    }
    if (!title.contains(".")) {
      return const Icon(Icons.folder);
    }
    return const Icon(Icons.text_snippet_sharp);
  }

  static String getFolderItem(String title, String type) {
    if (GlobalUtil.audioType.contains(type)) {
      return "Audios";
    }
    if (GlobalUtil.videoType.contains(type)) {
      return "videos";
    }
    if (GlobalUtil.pdfType.contains(type)) {
      return "pdfs";
    }
    if (GlobalUtil.wordType.contains(type)) {
      return "words";
    }
    if (GlobalUtil.exelType.contains(type)) {
      return "exels";
    }
    if (GlobalUtil.imageType.contains(type)) {
      return "images";
    }
    return "others";
  }

  static const  Set<String> audioType = {"mp3", "m3u"};
  static const  Set<String> videoType = {"mp4"};
  static const  Set<String> pdfType = {"pdf"};
  static const  Set<String> wordType = {"doc", "docx"};
  static const  Set<String> exelType = {"xls","xlsx"};
  static const  Set<String> imageType = {"jpg", "jpeg", "png", "gif"};
  static const  Set<String> folder = {"folder"};
  static const  String folderDocument = "driver";

  static const int  isAUDIOType = 1101;
  static const int  isVIDEOType = 1102;
  static const int isPDFType = 1103;
  static const int isWORDType = 1104;
  static const int isEXELType = 1105;
  static const int isIMAGEType = 1106;
  static const int isFOLDER = 1107;
  static const int isOther = 1108;


  static Future<void> onSelectFile({BuildContext? context}) async{
    final Directory appDocDir = await getApplicationDocumentsDirectory();
    String pathDir = appDocDir.path;
    pathDir = pathDir + "/" + GlobalUtil.folderDocument;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.any,
    );
    if(result != null){
      List<File> files = result.paths.map((path) => File(path!)).toList();
      for(int i = 0; i< files.length; i++){
        File item = files[i];
        String fileName = result.names[i] ?? "";
        String fileType = fileName
            .substring(fileName.lastIndexOf('.') + 1, fileName.length)
            .toLowerCase();
        String folderFile = GlobalUtil.getFolderItem(fileName, fileType);
        String outputPath  = "$pathDir/$folderFile/$fileName";
        Item fileItem  = Item(title: fileName,type: fileType,path:  outputPath,icon: GlobalUtil.getIconItem(fileName, fileType));
        File file = File(outputPath);
        await file.create(recursive: true).then(
                (value) =>  item.copy(outputPath).then((value) => _onSaveDatabase(fileItem,context!))
        );
      }
    }
  }

  static Future _onSaveDatabase(Item item, BuildContext context) async{
    final MyRepository _myRepository = MyRepositoryImpl();

    await _myRepository.insertSingleItem(item).then(
            (value) =>_myRepository.db.close());
  }

  // static Future _onSavePassword(User user, BuildContext context) async{
  //   final MyRepository _myRepository = MyRepositoryImpl();
  //   await _myRepository.savePassword(user).then(
  //           (value) =>_myRepository.db.close());
  // }

}

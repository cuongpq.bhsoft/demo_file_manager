

import 'package:demo_file_manager/BLOC/login/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreference{
  static Future<void> setTokenShared(String token) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

  static Future<String?> getTokenShared() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token =  prefs.getString('token');
    return token;
  }
  static Future<void> onLogout(BuildContext context) async{
    deleteTokenShared();
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LoginScreen(),));
  }
  static Future<void> deleteTokenShared() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('token');
  }

  static Future<void> onCheckTokenLocal(BuildContext context) async{
    String? token = await getTokenShared();
    if(token != null && token.isNotEmpty){
      return;
    }else{
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LoginScreen(),));
    }
  }
}